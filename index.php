<?php 
/**
 * Template Name: Home Page
 *
 * Gets the footer template
 */

get_header(); ?>

<!-- Full Screen image div -->





<div id="full">
    <div class="contain">
        <div class="box banner">
            
           
            
                <svg version="1.1" id="svg-logo"   x="0px" y="0px"
	  viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">
<circle style="opacity:0.8;fill:#121212;" cx="250.597" cy="250.097" r="200"/>
<g>
	<path class="path-b" fill="#none"  stroke="#ffffff" stroke-width="1" d="M74.094,323.909c0-0.44,0.389-0.661,1.171-0.661c0.312,0,1.252,0.195,2.816,0.588
		c1.564,0.389,3.988,0.781,7.271,1.175c3.283,0.39,7.267,0.587,11.959,0.587c26.269,0,48.392-5.629,66.373-16.895
		c17.978-11.261,26.97-24.296,26.97-39.105c0-19.85-19.56-29.774-58.677-29.774c-2.174,0-5.203,0.04-9.081,0.117
		c-3.881,0.081-6.675,0.118-8.383,0.118l-21.271,76.107c-4.62,0-8.401-1.12-11.346-3.36c-2.945-2.239-4.417-4.96-4.417-8.162
		c0-1.439,1.094-6.004,3.29-13.686c2.691-8.363,6.162-19.159,10.41-32.378c4.249-13.219,10.355-30.01,18.315-50.378
		c7.96-20.368,15.128-36.539,21.499-48.513c-5.894,1.116-11.346,2.754-16.362,4.906c-5.016,2.155-8.879,4.032-11.585,5.625
		c-2.71,1.597-4.38,2.394-5.016,2.394c-1.26,0-2.828-0.812-4.708-2.435s-2.82-3.007-2.82-4.146
		c6.929-2.82,12.462-4.972,16.604-6.462c4.138-1.487,9.87-2.938,17.195-4.348c7.326-1.41,14.89-2.115,22.692-2.115
		c6.367,0,12.337,0.47,17.911,1.41c5.574,0.94,11.387,2.582,17.438,4.928c6.051,2.35,10.865,6.184,14.449,11.504
		c3.584,5.324,5.375,11.82,5.375,19.487c0,24.267-17.94,39.336-53.815,45.208c35.72,2.218,53.58,14.952,53.58,38.209
		c0,10.671-3.606,20.49-10.81,29.457c-7.208,8.966-16.215,16.119-27.025,21.454c-10.81,5.331-21.855,9.47-33.135,12.407
		c-11.28,2.938-21.543,4.406-30.785,4.406C82.789,341.578,74.094,335.688,74.094,323.909z M115.924,234.418
		c9.242-0.308,17.896-1.311,25.967-3.007c8.067-1.696,15.745-4.245,23.03-7.641c7.285-3.393,13.043-8.1,17.273-14.118
		s6.345-13.039,6.345-21.065c0-20.361-13.241-30.543-39.715-30.543c-2.038,0-3.525,0.081-4.465,0.235
		C135.113,176.131,125.636,201.511,115.924,234.418z"/>
	<path class="path-w" fill="#none"  stroke="#ffffff" stroke-width="1" d="M217.209,194.938c0-10.024,4.384-19.505,13.16-28.435c8.772-8.93,18.719-13.395,29.845-13.395
		c9.554,0,16.763,3.724,21.62,11.163c4.854,7.443,7.285,17.666,7.285,30.667c0,13.006-2.549,28.553-7.639,46.647
		c-5.092,18.095-10.145,34.938-15.156,50.526c-5.016,15.59-7.52,26.988-7.52,34.191c0,6.11,2.35,9.166,7.049,9.166
		c7.363,0,16.41-6.662,27.143-19.976c10.729-13.314,20.053-28.354,27.965-45.12c7.91-16.762,12.572-30.392,13.982-40.89
		c4.699,0,8.225,1.216,10.574,3.643c2.352,2.431,3.525,5.053,3.525,7.873c0,2.666-1.41,7.366-4.229,14.1l-8.932,21.15
		c-3.289,7.365-6.268,15.709-8.93,25.027c-2.666,9.323-3.994,17.431-3.994,24.322c0,3.291,0.43,5.758,1.293,7.402
		c0.857,1.646,2.621,2.469,5.287,2.469c8.613,0,19.541-8.071,32.781-24.205c13.238-16.135,24.871-35.563,34.898-58.281
		c10.023-22.713,15.039-42.219,15.039-58.514c0-13.63-4.311-21.774-12.924-24.44c4.23-7.205,8.779-10.81,13.641-10.81
		c3.818,0,7.197,2.273,10.143,6.815c2.943,4.546,4.416,10.968,4.416,19.27c0,16.766-5.771,37.681-17.313,62.745
		c-11.545,25.068-25.557,47.118-42.043,66.153s-30.936,28.552-43.34,28.552c-5.17,0-9.832-1.527-13.982-4.583
		c-4.152-3.055-6.227-7.402-6.227-13.042c0-4.229,1.564-11.28,4.699-21.149c-18.33,25.85-34.544,38.774-48.645,38.774
		c-4.857,0-9.439-1.409-13.747-4.229c-4.311-2.82-6.462-7.678-6.462-14.57c0-5.952,2.467-16.686,7.402-32.195
		s9.87-32.547,14.805-51.112c4.936-18.565,7.402-34.582,7.402-48.058c0-4.384-0.041-7.792-0.117-10.222
		c-0.081-2.427-0.354-5.324-0.822-8.695c-0.471-3.367-1.176-5.912-2.115-7.637c-0.94-1.722-2.35-3.209-4.23-4.465
		c-1.88-1.252-4.152-1.88-6.814-1.88c-7.836,0-13.829,4.23-17.978,12.69c-4.153,8.46-6.228,17.39-6.228,26.79
		c0,5.17,0.705,9.4,2.115,12.69c-5.798,0-10.028-1.564-12.69-4.7C218.538,204.026,217.209,199.954,217.209,194.938z"/>
</g>
</svg>
                <h1>Ben Watherston</h1>
                <h2 class="banner-2">Front-end <span>//</span> UX Designer</h2>
                
                
                <a href="#portfolio">
                    <i class="fa fa-angle-down background-transition"></i>
                </a> 
        </div>
    </div>
</div>


		<?php 

            /* Limits posts in portfolio list to two */

            $p = array(
                'posts_per_page' => 2
            );
              
            query_posts($p);
            
            /* Starts the loop to fetch portfolio items */

            if ( have_posts() ) : 

        ?>
            <!-- List of two portfolio projects -->
            <ul class="portfolio">

        <?php
			
			while ( have_posts() ) : the_post();
                    
                get_template_part( 'template-parts/content', get_post_format() ); 
                
			/* Ends the loop of portfolio items */
			endwhile;
                
        ?> 

            </ul>

        <?php
            /* If no portfolio  */
            
            else :
                get_template_part( 'template-parts/content', 'none' );

            endif;
		?>

<section id="portfolio">
    <h2>View More Projects</h2>
    
    <div class="button background-transition colour-transition">
        <a href="portfolio">View</a>
    </div>
</section>

<section id="about">
    <h2>About</h2>
    <p>Ben is a 22-year-old Front-End Website Designer. Born and bred in the North West, based in the Liverpool area. Currently in his final year at Edge Hill University, studying BSc Web Systems Development.</p>
    
    <p>During his time at College he found his passion for Website Design and knew he wanted to further his skills in the field.</p>
    
    <p>5 years on and the devotion is still there for creating designs which are simple yet elegant.</p>

    <p>His skill set contains many different languages such as HTML5, CSS3, Less, Sass, JavaScript, jQuery, PHP, Responsive Design, WordPress, Email Design amongst other things.</p>

    <p>If you would like to get in touch, please click below.</p>

    <div class="button background-transition colour-transition">
        <a href="contact">Contact</a>
    </div>

</section>

<?php 
    /* Gets the footer template */
    get_footer(); 

?>