<?php 
/**
 * Template Name: Portfolio Page
 */
get_header(); ?>
<section id="page">
<h1>Portfolio</h1>

<?php 
    $p = array(
        'posts_per_page' => 100
);

    query_posts($p);
        if ( have_posts() ) : ?>

            <ul class="portfolio">

                <?php 
                    while ( have_posts() ) : the_post(); 
                        get_template_part( 'template-parts/content', get_post_format() ); 
                    endwhile;
                ?> 

            </ul> 

            <?php

                else :
                    get_template_part( 'template-parts/content', 'none' );
                endif;
            ?>
</section>
<?php get_footer(); ?>