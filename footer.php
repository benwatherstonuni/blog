    <footer>
        <div class="left">
            <a target="_blank" href="https://www.linkedin.com/in/benwatherston"><i class="fa fa-linkedin-square colour-transition"></i></a>
            <a target="_blank" href="https://twitter.com/BenWatherston"><i class="fa fa-twitter-square colour-transition"></i></a>
            <a target="_blank" href="https://plus.google.com/104706352075297251031/about"><i class="fa fa-google-plus-square colour-transition"></i></a>
            <a target="_blank" href="https://bitbucket.org/benwatherstonuni"><i class="fa fa-bitbucket-square colour-transition"></i></a>
        </div>
        <div class="right"><p>&copy; Ben Watherston, <?php echo date('Y'); ?></p></div>
        <div class="top"><a href="#home"><i class="fa fa-arrow-up"></i></a></div>
    
    </footer>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


    <script>
    $(document).ready(function(e) {
        
    
        /* Viewport change */
        
        $(window).scroll(function(){
            var scroll = $(window).scrollTop();
            var viewportHeight = $(window).height();
            
            if (scroll >= viewportHeight) {
                $("header").addClass("header-scroll");
                $("img.logo").addClass("logo-scroll");
                $(".logo-container").addClass("logo-container-scroll");
                $("nav ul").addClass("margin-none");
                $("#mobile-nav").addClass("nav-mobile");
                $("nav").addClass("main-nav-mobile");
            } else {
                $("header").removeClass("header-scroll");
                $(".logo").removeClass("logo-scroll");
                $(".logo-container").removeClass("logo-container-scroll");
                $("nav ul").removeClass("margin-none");
                $("#mobile-nav").removeClass("nav-mobile");
                $("nav").removeClass("main-nav-mobile");
            }
        });
        
        
    
        
        /* Mobile Navigation */
        
         $('.mobile-menu').click(function(){
            $('ul#menu-main-navigation').toggle();
         });
        
        
        
        /* Scroll smooth */
        
        $('a').click(function(){
            var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
                return false;
            }
        });
    
        
       
        
    
        });

    </script>


<?php wp_footer(); ?>

</body>
</html>