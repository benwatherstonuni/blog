<?php
/**
 * The template part for displaying content
 */
?>

<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
<?php 
    
    the_title( sprintf( '<a href="%s" rel="bookmark" class="portfolio-link">' . the_post_thumbnail('full', array('class' => 'portfolio-thumb')) . '<div class="portfolio-link-container background-transition opacity-transition"><h3>', esc_url( get_permalink() ) ), '</h3></div></a>' ); 
?>
    
</li>