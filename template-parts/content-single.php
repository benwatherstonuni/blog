<article id="post-<?php the_ID(); ?>">
		<?php the_title( '<h1>', '</h1>' ); ?>

		<?php
			the_content();
		?>
</article><!-- #post-## -->
