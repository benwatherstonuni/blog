<?php 
/**
 * Template Name: Contact Page
 */
get_header(); ?>
<section id="page" class="contact">
    <h1>Contact</h1>

<p>If you would like to get in touch with me, please email the following;</p>
   	<ul>
   		<li>22028048@go.edgehill.ac.uk</li>
   		<li>hello@benwatherston.co.uk</li>
   		<li>benwatherston@gmail.com</li>
   	</ul>

</section>


<?php get_footer(); ?>