<?php get_header(); ?>

<section id="page">
    
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'single' );

			// End of the loop.
		endwhile;
		?>


	<?php get_sidebar( 'content-bottom' ); ?>

</section><!-- .content-area -->






<?php get_footer(); ?>