<?php

/* Allows thumbnails for portfolio projects */

add_theme_support( 'post-thumbnails' ); 

/* Allows personalised Login page */

function my_custom_login() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.css" />';
}

add_action('login_head', 'my_custom_login');

/* Adds google analytics to the footer */

add_action('wp_footer', 'add_googleanalytics');

function add_googleanalytics() { ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73333184-1', 'auto');
  ga('send', 'pageview');

</script>
<?php } ?>