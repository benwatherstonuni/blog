<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ben Watherston</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php /*if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' );*/ ?>
        <link rel="icon" type="image/png" href="/images/favicon.png" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css" media="screen" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Montserrat:700,400' rel='stylesheet' type='text/css'>
        <?php wp_head(); ?>
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="http://www.ehustudent.co.uk/cis22028048/blog/">
                    <img src="<?php bloginfo('template_directory'); ?>/images/logo.png" />
                </a>
            </div>
                <?php wp_nav_menu(); ?>
                <a href="#" class="mobile-menu">Menu</a>
        </header>  

